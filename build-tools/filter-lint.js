const process = require('process');

process.stdin.setEncoding('utf8');

process.stdin.resume();

process.stdin.on('data', function (data) {
    const res = JSON.parse(data);

    res.errors.concat(res.warnings, res.notices).forEach(m =>
        process.stdout.write(`#${m._type}#${m.code}#${m.message}: ${m.description}#${m.file}#${m.line}#${m.column}\n`));
});
