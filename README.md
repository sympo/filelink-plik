# Filelink for Plik

A MailExtension for Thunderbird (68+) that uploads large attachments to [Plik](https://plik.root.gg) (or your own instance) and generates a link you can send by mail instead of the file.

Information for

* [Users](#user-content-user-guide)
* [Developers](#user-content-developer-guide)

## Requirements

* Thunderbird: 68.2.1 or newer

## User guide

### Installation

[__Filelink for Plik__](https://addons.thunderbird.net/de/thunderbird/addon/filelink-for-plik/) is available via Thunderbird's Add-on repository.

1. Install it directly from the Add-ons management within Thunderbird.
1. Go to Settings -> Attachments -> Sending and add __Plik__.

### Upload Token

If you have an account on your Plik instance, you can add your uploads to it via a Upload Token.

1. log into it
1. click on "Tokens"
1. create a token
1. paste it into the Upload Token field

### Known issues

#### Files from network shares uploaded to Plik *and* attached

There was a [bug in
Thunderbird](https://bugzilla.mozilla.org/show_bug.cgi?id=793118): If you
attached a file from a network share, it was uploaded to Plik and the share
link was inserted into your mail, but the file was *also attached to the
message*. This was fixed in Thunderbird 68.11.0 and 78.0.1. If you're still
experiencing this issue, update Thunderbird.

## Developer guide

The project lives on GitLab: <https://gitlab.com/joendres/filelink-plik>. If
you'd like to contribute to the project, help with testing on different
platforms, have a feature suggestion or any other comment, just contact
[me](@joendres).

### Dev resources

* Plik [API documentation](https://github.com/root-gg/plik/blob/master/documentation/api.md)
* [Thunderbird WebExtension
  APIs](https://thunderbird-webextensions.readthedocs.io/en/latest/index.html)
* [Example extensions for Thunderbird WebExtensions
  APIs](https://github.com/thundernest/sample-extensions)
* [What you need to know about making add-ons for
  Thunderbird](https://developer.thunderbird.net/add-ons/), not complete at all.
* [Getting started with
  web-ext](https://extensionworkshop.com/documentation/develop/getting-started-with-web-ext)
  If you are developing WebExtensions, you want to use this tool. For debugging
  just set the ```firefox``` config option to your thunderbird binary.

## Contributors

* [Johannes Endres](@joendres), initial implementation, maintainer
* [Savek-CC](@savek-cc), Upload Token
